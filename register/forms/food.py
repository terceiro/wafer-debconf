from django import forms

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Field, Layout

from invoices.prices import MEAL_PRICES
from register.dates import conference_dinner_meal, meal_choices
from register.models.food import Food
from register.fields import MealSelectionField


class FoodForm(forms.Form):
    meals = forms.MultipleChoiceField(
        label='I want to eat catered food for these meals:',
        choices=meal_choices(),
        widget=forms.CheckboxSelectMultiple,
        help_text="If you don't have a food bursary, meal prices are: "
                  "Breakfast {breakfast} USD, Lunch {lunch} USD, Dinner "
                  "{dinner} USD.".format(**MEAL_PRICES),
        required=False,
    )
    conference_dinner = forms.BooleanField(
        label='I want to attend the conference dinner',
        help_text="If you don't have a food bursary, the conference dinner "
                  "will cost {conference_dinner} USD.".format(**MEAL_PRICES),
        required=False,
    )
    diet = forms.ChoiceField(
        label='My diet',
        choices=Food.DIETS.items(),
        required=False,
    )
    special_diet = forms.CharField(
        label='Details of my special dietary needs',
        required=False,
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.include_media = False
        conference_dinner = ()
        if conference_dinner_meal():
            conference_dinner = (Field('conference_dinner'),)
        self.helper.layout = Layout(
            MealSelectionField('meals', id='meals'),
            *conference_dinner,
            Field('diet', id='diet'),
            Field('special_diet', id='special_diet'),
        )

    def clean(self):
        cleaned_data = super().clean()

        if cleaned_data.pop('conference_dinner'):
            cleaned_data['meals'].append(conference_dinner_meal())

        if (cleaned_data.get('diet') == 'other' and
                not cleaned_data.get('special_diet')):
            self.add_error('special_diet', 'Required when diet is "other"')
